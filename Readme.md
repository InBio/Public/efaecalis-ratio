Simulation code for *insert paper reference once published*.

Code originally used in the paper used a custom ODE solving library for efficiency reasons. 
This code has been modified to rely on common python packages and be easier to run on different setups.
To add reproducibility, intermediate results of simulations used in plots have been saved to CSV files
that you can load instead of running the simulations.

Running the notebook only requires that you install Python 3.6+, jupyter and the imported packages using pip or conda.


CSVs layout
============

fraction_time_traces.csv
-----------------------

Used to generate figure *S7*.

This CSV contains fractions of recipients at various time points for different initial conditions and is organized as follow:

- First row: 
  - first cell: empty,
  - then time-traces number
- Second row: 
  - first cell: "pop0",
  - remaining cells: population size for each time trace
- Third row:
  - first cell: "r-frac0",
  - remaining cells: initial fraction of recipients for each time trace
- Remaining rows:
  - first cell: time in hours
  - remaining cells: fraction of recipients at said time

dfss.csv
---------

Used to generate figure *4*.

This CSV contains the steady state value of several variables for random initial conditions, for various sensing strategies, and values of parameter K ("carrying capacity"). The columns are named as follow:

- *d*: concentration of donors at steady state
- *r*: concentration of recipients at steady state
- *K*: value of parameter K
- *Strategy*: name of the sensing stratedy
- *p*: d+r, the concentration of the total population at steady state
- *f*: r/p, the fraction of recipients at steady-state
- *fdon*: d/p: the fraction of donors at steady state

For each column, the first row is the column's name.

dfrob.csv
--------

Used to generate firgure *S8*.

This CSV contains the steady state value of several variables for various sensing strategies, strategy's parameters, and values of parameter K ("carrying capacity"). The columns are named as follow:

- *d*: concentration of donors at steady state
- *fd*: the fraction of donors at steady state
- *kind*: numerical value indicating the sensing strategy:
    `{0: 'Ratio-sensing', 1: 'Constitutive activation', 2: 'Recipients-density-sensing', 3: 'Population-size-sensing'}`
- *k*: value of parameter K
- *theta,a,r50,p50*: each one is the strategy parameter for one strategy
- *hue*: a user friendly string rassembling information on the strategy and the parameter value for this strategy
- *maxdatk*: maximum donor concentration at steady-state reached across strategies for this given K
- *ratio*: d/maxdatk, the normalized donor concentration at steady state
- *lk*: log10(K)
